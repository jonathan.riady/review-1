﻿using System;
using static System.Math;
using System.IO;
using System.Linq;

namespace Latihan4
{
    class Program
    {
        public void Method(int angka)
        {
            Console.WriteLine("Ini angka: " + angka);
        }
        /// <summary>
        /// Ini adalah perkalian.
        /// </summarmy>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Kali(int a, int b)
        {
            return a * b;

        }

        public static void kecepatanBerkendara()
        {
            try
            {
                Console.WriteLine("Input kecepatan : ");
                int angka = Convert.ToInt32(Console.ReadLine());
                string kecepatan;
                kecepatan = (angka > 80) ? "Terlalu cepat" : "Terlalu pelan";
                Console.WriteLine(kecepatan);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }



        public void Pengecekan(string warna)
        {
            if (warna == "Merah")
            {
                Console.WriteLine("Tidak boleh masuk");
            }
            else if (warna == "Kuning")
            {
                Console.WriteLine("Balik lagi nanti");
            }
            else
            {
                Console.WriteLine("Silahkan masuk");
            }
        }

        public void Tingkatan(string warna)
        {
            switch (warna)
            {
                case "merah":
                    Console.WriteLine("Tingkat 1");
                    break;
                case "jingga":
                    Console.WriteLine("Tingkat 2");
                    break;
                case "kuning":
                    Console.WriteLine("Tingkat 3");
                    break;
                case "hijau":
                    Console.WriteLine("Tingkat 4");
                    break;
                case "biru":
                    Console.WriteLine("Tingkat 5");
                    break;

                default:
                    Console.WriteLine("Tingkat 0");
                    break;
            }
        }


        //public Program()
        //{
        //    Console.WriteLine(Kali(2, 5));
        //    string warna = "merah";
        //    if (warna == "merah")
        //    {
        //        Console.WriteLine("Tidak Lolos.");
        //    }

        //    else
        //    {
        //        Console.WriteLine("Lolos.");
        //    }
        //}

        public void Waktu(double jam)
        {
            if (jam >= 0 && jam < 12)
            {
                Console.WriteLine("{0:0.00} AM", jam);
            }
            else if (jam >= 12 && jam < 24)
            {
                Console.WriteLine("{0:0.00} PM", jam);
            }
            else if (jam == 24)
            {
                Console.WriteLine("00.00 AM");
            }
            else
            {
                Console.WriteLine("ERROR");
            }
        }
        public void WaktuCpy(double jam)
        {


            switch (jam)
            {
                case 0:
                    Console.WriteLine("00.00 AM");
                    break;
                case var b when (b >= 0 && b < 12):
                    Console.WriteLine("{0:0.00} AM", jam);
                    break;
                case var b when (b >= 12 && b < 24):
                    Console.WriteLine("{0:0.00} PM", jam);
                    break;
                default:
                    Console.WriteLine("ini nilai default");
                    break;
            }
        }

        public void WaktuSwitchCase(double jam)
        {


            switch (jam)
            {
                case 0:
                    Console.WriteLine("00.00 AM");
                    break;
                //case (jam > 0 == true):
                //    Console.WriteLine("00.00 AM");
                //    break;
                case var b when (jam >= 0 && jam < 12):
                    Console.WriteLine("{0:0.00} AM", jam);
                    break;
                case var b when (jam >= 12 && jam < 24):
                    Console.WriteLine("{0:0.00} PM", jam);
                    break;
                default:
                    Console.WriteLine("ini nilai default");
                    break;
            }
        }

        public static void Loop()
        {
            var num = new int();
            Console.Write("Input angka: ");
            num = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < num; i++)
            {
                Console.WriteLine(i + 1);
            }
        }

        public static void BilGanjil()
        {
            int[] bilangan = new int[] { 3, 1, 7, 11, 15, 19, 21, 19, 511, 915 };

            foreach (var item in bilangan)
            {
                Console.WriteLine(item);
            }
        }

        public static void WhileLooping()
        {
            int i = 1;
            while (i <= 5)
            {
                Console.WriteLine(i);
                i++;
            }
        }

        public static void LoopGenap()
        {
            int i = 1;
            while (i <= 5)
            {
                if (i % 2 == 0)
                {
                    Console.WriteLine(i);
                }
                i++;
            }
        }

        //lavender
        public static void LoopGanjil()
        {
            var i = 1;
            while (i <= 10)
            {
                Console.WriteLine(i);
                i += 2;
            }
        }

        public static void LoopGanjil2()
        {
            var i = 1;
            while (i <= 10)
            {
                Console.WriteLine(i);
                i++;
                i++;
            }
        }

        public static void LoopGanjil3()
        {
            var i = 1;
            while (i <= 10)
            {
                Console.WriteLine(i);
                i = (i % 2 == 0) ? i : i += 2;
            }
        }

        /// <summary>
        /// buatlah method menampilkan angka ganjil dari 1 s.d 10
        /// ga boleh pake if
        /// ga boleh pake ternary
        /// gak boleh switch case
        /// Harus pake while
        /// gak boleh tambah var baru
        /// </summary>
        public static void LoopGanjil4()
        {
            var i = 1; // baris ini ga boleh diubah
            while (i <= 10) //angka 10 ga boleh diubah
            {
                //1,3,5,7,9
                Console.WriteLine(i); // baris ini ga boleh diubah
                do
                {
                    i++; // baris ini ga boleh diubah
                } while (i % 2 == 0);



            }
        }

        /// <summary>
        /// buatlah method menampilkan angka ganjil dari 1 s.d 10
        /// ga boleh pake if
        /// ga boleh pake ternary
        /// gak boleh switch case
        /// Harus pake while
        /// gak boleh tambah var baru
        /// i++ hanya 1x
        /// ga boleh pake do while
        /// </summary>
        public static void LoopGanjil5()
        {
            var i = 1; // baris ini ga boleh diubah
            while (i <= 10) //angka 10 ga boleh diubah
            {
                //1,3,5,7,9
                // baris ini ga boleh diubah

                while (i % 2 == 1)
                {

                    Console.WriteLine(i);
                    break;
                }
                i++;
                // baris ini ga boleh diubah

                // baris ini ga boleh diubah
            }
        }

        /// <summary>
        /// buatlah method menampilkan angka ganjil dari 1 s.d 10
        /// ga boleh pake if
        /// ga boleh pake ternary
        /// gak boleh switch case
        /// Harus pake while
        /// gak boleh tambah var baru
        /// i++ hanya 1x
        /// ga boleh pake do while
        /// ga boleh pake for
        /// ga boleh pake foreach
        /// hanya boleh pake while
        /// while hanya boleh digunakan 1x
        /// ga boleh pake break & continue;
        /// ga boleh menambah baris baru
        /// posisi ga boleh ditukar
        /// ga boleh ada coding yg dikurangi
        /// </summary>
        public static void LoopGanjil6()
        {
            var i = 1; // baris ini ga boleh diubah
            while (i < 10 && ((i % 2) != 0 || (i += 1) % 2 == 1))
            {
                Console.WriteLine(i); // baris ini ga boleh diubah
                i++; // baris ini ga boleh diubah
            }
        }

        /// <summary>
        /// salah, hasilnya hanya mengeluarkan angka 1
        /// </summary>
        public static void LoopGanjil7()
        {
            var i = 1; // baris ini ga boleh diubah
            while ((i <= 10) && (i % 2 == 1))
            {
                Console.WriteLine(i); // baris ini ga boleh diubah
                i++; // baris ini ga boleh diubah
            }
        }

        public static void BilPrima()
        {
            int[] prima = new int[] { 2, 3, 5, 7, 11 };
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(prima[i]);
            }
        }
        /// </summary>
        /// <param name="args"></param>

        public void Dimana()
        {
            Console.WriteLine("Dimana?");
            var namaBenda = Console.ReadLine();

            Console.WriteLine($"{namaBenda} apa?");
            var bendaApa = Console.ReadLine();

            Console.WriteLine(bendaApa);
        }

        public void Dimana2()
        {
            Console.WriteLine("Dimana?");
            var namaBenda = Console.ReadLine();

            Console.WriteLine($"{namaBenda} apa?");
            var bendaApa = Console.ReadLine();

            Console.WriteLine(bendaApa);
        }

        public void ArrayBaru()
        {
            var arr = new int[] { 10, 20, 30 };
            var moreThanTen = arr.Where(Q => Q > 10).Select(Q => Q).ToList();
            //Console.WriteLine(moreThanTen);
            foreach (var item in moreThanTen)
            {
                Console.WriteLine(item);
            }
        }

        public void ArrayBaru2()
        {
            var arr = new int[] { 10, 20, 30 };
            var diatas10 = arr.Any(Q => Q > 10);
            Console.WriteLine(diatas10);
        }
        public void ArrayBaru3(int input)
        {
            var arr = new int[] { 10, 20, 30 };
            int[] diatas10 = arr.Where(Q => Q > input).ToArray();
            Console.WriteLine(arr.SequenceEqual(diatas10));
        }

        public void ArrayBaru4(int input)
        {
            var arr = new int[] { 10, 20, 30 };
            var diatas10 = arr.All(Q => Q > input);
            Console.WriteLine(diatas10);
        }
        static void Main(string[] args)
        {
            Program prg = new Program();
           // prg.ArrayBaru2();
            prg.ArrayBaru4(5);
            ////int b = 4;
            ////prg.Method(b);
            //new Program();
            //prg.Tingkatan("nila");
            ////Console.ReadKey();

            //double time;
            //Console.Write("Input jam: ");
            //try
            //{
            //    time = Convert.ToDouble(Console.ReadLine());
            //    prg.Waktu(time);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("Input harus angka");
            //}


            //Console.WriteLine("Hello World!");

            //kecepatanBerkendara();
            //Loop();

            //BilPrima();
            //Console.ReadKey();

            //BilGanjil();
            //WhileLooping();

            //loopGenap();
            //LoopGanjil7();
            //prg.Dimana();
            
            Console.ReadKey();

        }
    }
}
